from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(MasterKategori)
admin.site.register(MasterArtikel)
admin.site.register(File)