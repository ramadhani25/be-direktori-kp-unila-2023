from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from beapjiiapps.models import *
from beapjiiapps.serializers import *
from beapjiiapps.views import *
from rest_framework.pagination import PageNumberPagination
from beapjiiapps.pagination import CustomPagination
from django.http import HttpResponse
from django.core.files.storage import FileSystemStorage
import os

@api_view(["GET"])
@permission_classes((IsAdminUser,))
def getUser(request, pk):
    item = User.objects.get(pk=pk)
    serializer = UserSerializer(
        item, context={"request": request}, many=False
    )
    return Response(serializer.data)

@api_view(["POST"])
def login(request):
    data = request.data
    user = authenticate(username=data["username"], password=data["password"])
    if user is not None:
        token, created = Token.objects.get_or_create(user=user)
        return Response({"status": 200, "messages": "Login Berhasil!", "token": token.key})
    return Response({"status": 400, "messages": "Login Gagal!"})

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def logout(request):
    if request.user.is_authenticated:
        try:
            Token.objects.filter(user=request.user).delete()
            return Response({'status': 200, 'message': 'Logout successful'})
        except Exception as e:
            return Response({'status': 400, 'message': 'Logout failed'})
    else:
        return Response({'status': 400, 'message': 'Invalid token'})

@api_view(["POST"])
@permission_classes((IsAdminUser,))
def create(request):
    data = request.data
    create_user = User.objects.create(
        username = data["username"],
        password = make_password(data["password"]),
        email = data["email"],
    )
    create_user.save()
    return Response({"status": 200, "messages": "User berhasil dibuat!"})
    

@api_view(["PUT", "DELETE"])
@permission_classes((IsAdminUser,))
def edelUser(request, pk):
    data = request.data
    master = User.objects.get(pk=pk)

    if request.method == "PUT":
        master.email = data["email"]
        master.username = data["username"]
        master.password = make_password(data["password"])
        master.save()
        return Response({"status": 200, "messages": "Data Berhasil Diubah!"})
    elif request.method == "DELETE":
        master.delete()
        return Response({"status": 200, "messages": "Data Berhasil Dihapus!"})
    
# MASTER KATEGORI
@api_view(["GET"])
def getKategori(request, pk):
    item = MasterKategori.objects.get(pk=pk)
    serializer = MasterKategoriSerializer(
        item, context={"request": request}, many=False
    )
    return Response(serializer.data)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def createKategori(request):
    data = request.data

    create = MasterKategori.objects.create(
        nama=data["nama"],
        created_by=request.user,
        updated_by=request.user
    )
    create.save()
    return Response({"status": 200, "messages": "Data Berhasil Ditambah!"})

@api_view(["PUT", "DELETE"])
@permission_classes((IsAuthenticated,))
def edelKategori(request, pk):
    data = request.data
    master = MasterKategori.objects.get(pk=pk)

    if request.method == "PUT":
        master.nama = data["nama"]
        master.updated_by = request.user
        master.save()
        return Response({"status": 200, "messages": "Data Berhasil Diubah!"})
    elif request.method == "DELETE":
        master.delete()
        return Response({"status": 200, "messages": "Data Berhasil Dihapus!"})
    
# ARTIKEL

@api_view(["GET"])
def getArtikel(request, pk):
    item = MasterArtikel.objects.get(pk=pk)
    item.view_counter += 1
    item.save()
    serializer = ArtikelSerializer(
        item, context={"request": request}, many=False
    )
    return Response(serializer.data)

@api_view(["GET"])
def getArtikelbyKategori(request, kategori_id):
    artikels = MasterArtikel.objects.filter(kategori_id=kategori_id)
    read_count = 0
    for artikel in artikels:
        read_count += artikel.view_counter
    paginator = PageNumberPagination()
    paginator.page_size = 10
    paginated_items = paginator.paginate_queryset(artikels, request)
    serializer = ArtikelSerializer(
        paginated_items, context={"request": request}, many=True
    )
    paginated_response = paginator.get_paginated_response(serializer.data)
    response_data = paginated_response.data
    response_data['total_views'] = read_count

    return Response(response_data)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def createArtikel(request):
    data = request.data
    banner = request.FILES.get("banner")
    if banner is None:
        create_artikel = MasterArtikel.objects.create(
            judul_artikel = data["judul"],
            kategori_id = data["kategori"],
            deskripsi = data["deskripsi"],
            author = request.user,
        )
        create_artikel.save()
        return Response({"status": 200, "artikel_id": create_artikel.pk, "messages": "Data Berhasil Ditambah!"})
    else:
        base_filename, extension = os.path.splitext(banner.name)
        file_name = base_filename + extension
        fs = FileSystemStorage()
        if extension in ['.png', '.jpg', '.jpeg', '.HEIC']:
            folder_name = 'image'
            file_path = fs.save(os.path.join(folder_name, file_name), banner)
        else:
            raise ValueError('Only image file is allowed.')
        
        create_artikel = MasterArtikel.objects.create(
            judul_artikel = data["judul"],
            kategori_id = data["kategori"],
            deskripsi = data["deskripsi"],
            author = request.user,
        )

        create_file = File.objects.create(
            artikel_id = create_artikel.pk,
            judul_file = base_filename,
            file = file_path,
            uploaded_by = request.user,
        )

        create_artikel.banner_id = create_file.id
        create_artikel.save()
        create_file.save()
        return Response({"status": 200, "artikel_id": create_artikel.pk, "messages": "Data Berhasil Ditambah!"})

@api_view(["PUT", "DELETE"])
@permission_classes((IsAuthenticated,))
def edelArtikel(request, pk):
    master = MasterArtikel.objects.get(pk=pk)
    fs = FileSystemStorage()
    if request.method == "PUT":
        data = request.data
        uploaded_file = request.FILES.get('banner')
        if uploaded_file is not None:
            try:
                old_banner = File.objects.get(pk=master.banner_id)
                if old_banner is not None:
                    fs.delete(str(old_banner.file))
                    old_banner.delete()
            except:
                pass
            base_filename, extension = os.path.splitext(uploaded_file.name)
            file_name = base_filename + extension
            if extension in ['.png', '.jpg', '.jpeg', '.HEIC']:
                folder_name = 'image'
                file_path = fs.save(os.path.join(folder_name, file_name), uploaded_file)
                new_banner = File.objects.create(
                    artikel_id = master.pk,
                    judul_file = base_filename,
                    file = file_path,
                )
            else:
                raise ValueError('Only pdf and image files are allowed.')
            master.banner = new_banner
        master.judul_artikel = data["judul"]
        master.kategori_id = data["kategori"]
        master.deskripsi = data["deskripsi"]
        master.updated_by = request.user
        master.save()
        return Response({"status": 200, "messages": "Data Berhasil Diubah!"})
    elif request.method == "DELETE":
        try:
            all_file = File.objects.filter(artikel_id=pk)
            print(all_file)
            if all_file.exists():
                for file in all_file:
                    fs.delete(str(file.file))
                    file.delete()
        except:
            pass
        master.delete()
        return Response({"status": 200, "messages": "Data Berhasil Dihapus!"})

# FILE
    
@api_view(["GET"])
def getFile(request, pk):
    item = File.objects.get(pk=pk)
    serializer = FileSerializer(
        item, context={"request": request}, many=False
    )
    return Response(serializer.data)

@api_view(["GET"])
def getFilebyArtikel(request, artikel_id):
    item = File.objects.filter(artikel=artikel_id)
    
    paginator = CustomPagination()
    paginated_items = paginator.paginate_queryset(item, request)
    
    serializer = FileSerializer(paginated_items, context={"request": request}, many=True)
    response_data = serializer.data
    
    return paginator.get_paginated_response(response_data)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def uploadFile(request):
    data = request.data
    artikel_id = data["artikel"]
    old_files = File.objects.filter(artikel_id=artikel_id)
    total_file = 0
    if not old_files.exists():
        total_file = 0
    else:
        for old_file in old_files:
            file_extension = str(old_file.file).split(".")[-1]
            if file_extension == "pdf":
                total_file +=1
    fs = FileSystemStorage()
    i = total_file
    files = request.FILES.getlist("files")
    for file in files:
        base_filename, extension = os.path.splitext(file.name)
        if extension in ['.pdf', '.png', '.jpg', '.jpeg', '.HEIC']:
            if extension == '.pdf':
                i += 1
                judul_file = base_filename + "(" + str(i) +")"
                file_name = judul_file + extension
                folder_name = 'file'
            else:
                judul_file = base_filename
                file_name = judul_file + extension
                folder_name = 'image'
        else:
            raise ValueError('Only pdf and image files are allowed.')
        
        file_path = fs.save(os.path.join(folder_name, file_name), file)
        create = File.objects.create(
            artikel_id = data["artikel"],
            judul_file = judul_file,
            file = file_path,
            uploaded_by = request.user,
        )
        create.save()
    return Response({"status": 200, "messages": "Data Berhasil Ditambah!"})

@api_view(["GET"])
def download_file(request, pk):
    file = get_object_or_404(File, pk=pk)

    # Tingkatkan nilai counter sebelum mengirim file
    file.download_counter += 1
    file.save()
    filename = str(file.file).split("/")[-1]
    print(filename)

    # sending response 
    response = HttpResponse(file.file, content_type=['application/pdf', 'image/png', 'image/jpg', 'image/jpeg', 'image/HEIC'])
    response['Content-Disposition'] = f'attachment; filename="{filename}"'
    return response

@api_view(["DELETE"])
@permission_classes((IsAuthenticated,))
def deleteFile(request, pk):
    master = File.objects.get(pk=pk)
    fs = FileSystemStorage()
    fs.delete(str(master.file))
    master.delete()
    return Response({"status": 200, "messages": "Data Berhasil Dihapus!"})