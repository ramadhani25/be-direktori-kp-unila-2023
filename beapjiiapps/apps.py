from django.apps import AppConfig
import os
from django.conf import settings

class BeapjiiappsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'beapjiiapps'

    def ready(self):
        # This code will run when the server starts
        folder_names = ['file', 'image']

        for folder_name in folder_names:
            folder_path = os.path.join(settings.MEDIA_ROOT, folder_name)
            if not os.path.exists(folder_path):
                os.makedirs(folder_path)