from django.db import models
from django.contrib.auth.models import User

class MasterKategori(models.Model):
    nama = models.CharField(max_length=200, null=True)
    created_by = models.ForeignKey(User, on_delete=models.SET_NULL, null= True, related_name='kategori_created_at')
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='kategori_updated_at')
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    def __str__(self):
        return self.nama
    
class MasterArtikel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)
    judul_artikel = models.CharField(max_length=200, null=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    kategori = models.ForeignKey(MasterKategori, on_delete=models.SET_NULL, null=True)
    deskripsi = models.TextField(null=True)
    banner = models.ForeignKey('File' ,blank= True, on_delete=models.SET_NULL, null=True)
    updated_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='artikel_updated_by')
    view_counter = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.judul_artikel
    
class File(models.Model):
    artikel = models.ForeignKey(MasterArtikel, on_delete=models.SET_NULL, null=True)
    judul_file = models.CharField(max_length=200, null=True)
    file = models.FileField(null=True)
    download_counter = models.PositiveIntegerField(default=0)
    uploaded_by = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name='file_uploaded_by')

    def __str__(self):
        return self.judul_file