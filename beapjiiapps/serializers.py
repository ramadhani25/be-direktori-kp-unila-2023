from rest_framework import serializers
from django.contrib.auth.models import User
from .models import *

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ["id","username","email"]
        depth = 1

class MasterKategoriSerializer(serializers.ModelSerializer):
    created_by = UserSerializer(read_only=True)
    updated_by = UserSerializer(read_only=True)
    class Meta:
        model = MasterKategori
        fields = "__all__"
        depth = 1

class ArtikelSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    updated_by = UserSerializer(read_only=True)
    class Meta:
        model = MasterArtikel
        fields = "__all__"
        depth = 1

class FileSerializer(serializers.ModelSerializer):
    uploaded_by = UserSerializer(read_only=True)
    class Meta:
        model = File
        fields = "__all__"
        depth = 1