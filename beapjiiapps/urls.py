from django.urls import path

from beapjiiapps.api import masterdata, views

urlpatterns = [
    #MASTER KATEGORI
    path("kategori/<int:pk>", masterdata.getKategori, name="getKategori"),
    path("kategori/create", masterdata.createKategori, name="createkategori"),
    path("kategori/edelkategori/<int:pk>", masterdata.edelKategori, name="edelkategori"),
    path("kategori", views.MasterKategoriView.as_view(), name="getdatakategori"),

    #ARTIKEL
    path("artikel/<int:pk>", masterdata.getArtikel, name="getArtikel"),
    path("artikel/kategori/<int:kategori_id>", masterdata.getArtikelbyKategori, name="getArtikelbyKategori"),
    path("artikel/create", masterdata.createArtikel, name="createArtikel"),
    path("artikel/edelartikel/<int:pk>", masterdata.edelArtikel, name="edelArtikel"),
    path("artikel", views.ArtikelView.as_view(), name="getdataArtikel"),

    #FILE
    path("file/download/<int:pk>", masterdata.download_file, name="downloadFile"),
    path("file/<int:pk>", masterdata.getFile, name="getFile"),
    path("file/artikel/<int:artikel_id>", masterdata.getFilebyArtikel, name="getFilebyArtikel"),
    path("file/upload", masterdata.uploadFile, name="uploadFile"),
    path("file/delete/<int:pk>", masterdata.deleteFile, name="deleteFile"),
    path("file", views.FileView.as_view(), name="getdataFile"),

    #AUTH
    path("user/<int:pk>", masterdata.getUser, name="getUser"),
    path("user/create", masterdata.create, name="create"),
    path("user/login", masterdata.login, name="login"),
    path("user/logout", masterdata.logout, name="logout"),
    path("user/edeluser/<int:pk>", masterdata.edelUser, name="edeluser"),
    path("user", views.UserView.as_view(), name="getdatauser"),

]